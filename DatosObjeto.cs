﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompeticionClicks
{
    
    public class DatosObjeto
    {
        public enum Edades { BABY, ADULTO, ANCIANO };
        public Utiles.TipoBoton Tipo { get; set; }
        public string Edad { get; set; }
        public bool Infectado { get; set; }
        public Timer TiempoInfectado { get; set; }
        public Timer TiempoHospitalSaturado { get; set; }
        public int SegundosCuracionHospital { get; set; }
        public bool HospitalDisponible { get; set; }
        public Point DireccionAnterior { get; set; }
        public string Guid { get; set; }
        public int Id { get; set; }

    }
}
