﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompeticionClicks
{
    public partial class FrmClasificaciones : Form
    {

        CLASIFICACIONES[] Clasif { get; set; }



        public FrmClasificaciones()
        {
            InitializeComponent();
        }


        public class Clasificacion
        {
            public DateTime Fecha { get; set; }          
            public string Nombre { get; set; }
            public int Puntos { get; set; }
            public string Dificultad { get; set; }
        }


        private CLASIFICACIONES[] LeerClasificacionBD()
        {
            using(BDEntities db = new BDEntities())
            {
                List<CLASIFICACIONES> ret = (from cla in db.CLASIFICACIONES select cla).ToList();
                return ret.ToArray();
            }           
        }

        private Clasificacion[] LeerClasificacion()
        {
            //Leemos el fichero completo en una string:
            string fichero = File.ReadAllText(Utiles.RutaFichero);

            //Lo partimos en lineas y lo guardamos en un array de strings:
            string[] lineas = fichero.Split('\n');

            //declaro la variable que vamos a devolver:
            List<Clasificacion> ret = new List<Clasificacion>();

            //foreach guardando los datos en el ret:
            foreach(string linea in lineas)
            {
                Clasificacion lin = new Clasificacion();
                lin.Fecha = Convert.ToDateTime(linea.Split(';')[0]);
                lin.Nombre = linea.Split(';')[1];
                lin.Puntos = Convert.ToInt32(linea.Split(';')[2]);
                lin.Dificultad = linea.Split(';')[3];
                ret.Add(lin);
            }

            return ret.ToArray();
        }

        private void FrmClasificaciones_Load(object sender, EventArgs e)
        {

            if (File.Exists(Utiles.RutaFichero))
            {
                Clasif = LeerClasificacionBD();
                dgvClasificacion.AllowUserToAddRows = false;
                dgvClasificacion.DataSource = Clasif;
                if (dgvClasificacion.Columns.Contains("ID")) dgvClasificacion.Columns["ID"].Visible = false;
                OrdenarPorMasRapidos();
            }
            else
            {
                dgvClasificacion.DataSource = "NO HAY CLASIFICACION";
            }
            
            
        }

        private void OrdenarPorMasRapidos()
        { 
            //Borramos el dgv por si tiene datos de antes:
            dgvClasificacion.DataSource = null;
            //Le metemos como source de datos el array de clases "Clasificacion" ordenadas como queremos.
            dgvClasificacion.DataSource = Clasif.OrderByDescending(o => o.PUNTUACION).ToArray();
            if (dgvClasificacion.Columns.Contains("ID")) dgvClasificacion.Columns["ID"].Visible = false;

            if(dgvClasificacion.Rows.Count > 0) dgvClasificacion.Rows[0].Cells["Nombre"].Value = "SUPERDANI";


        }

        private void btnMasRapidos_Click(object sender, EventArgs e)
        {
            OrdenarPorMasRapidos();
        }

        private void btnMasNuevos_Click(object sender, EventArgs e)
        {
            dgvClasificacion.DataSource = null;
            dgvClasificacion.DataSource = Clasif.OrderByDescending(o => o.FECHA).ToArray();
            if (dgvClasificacion.Columns.Contains("ID")) dgvClasificacion.Columns["ID"].Visible = false;

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            string passwordOK = "JuliA";
            if(txtPassword.Text == passwordOK)
            {
                //File.Delete(Utiles.RutaFichero);
                using(BDEntities dbLeer = new BDEntities())
                using(BDEntities dbBorrar = new BDEntities())
                {
                    var existentes = from c in dbLeer.CLASIFICACIONES select c;
                    foreach(CLASIFICACIONES cla in existentes)
                    {
                        dbBorrar.Entry(cla).State = System.Data.Entity.EntityState.Deleted;
                        dbBorrar.SaveChanges();
                    }


                }



                txtPassword.Text = "";
                MessageBox.Show("Clasificaciones borradas!", "Borrado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Vete a suplantar a tu prima", "Sin password", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
