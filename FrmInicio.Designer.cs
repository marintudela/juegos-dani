﻿namespace CompeticionClicks
{
    partial class FrmInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numDemonios = new System.Windows.Forms.NumericUpDown();
            this.numFantasmas = new System.Windows.Forms.NumericUpDown();
            this.numSegundos = new System.Windows.Forms.NumericUpDown();
            this.btnJuego = new System.Windows.Forms.Button();
            this.chkJugarConBonus = new System.Windows.Forms.CheckBox();
            this.cmbDificultad = new System.Windows.Forms.ComboBox();
            this.chkAñadirTiempo = new System.Windows.Forms.CheckBox();
            this.chkUsarMovimiento = new System.Windows.Forms.CheckBox();
            this.rbLentos = new System.Windows.Forms.RadioButton();
            this.rbRapidos = new System.Windows.Forms.RadioButton();
            this.rbStandard = new System.Windows.Forms.RadioButton();
            this.chPintura = new System.Windows.Forms.CheckBox();
            this.btnCorona = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chkEnLinea = new System.Windows.Forms.CheckBox();
            this.chkAlAzar = new System.Windows.Forms.CheckBox();
            this.numTamañoCacharro = new System.Windows.Forms.NumericUpDown();
            this.numPersonas = new System.Windows.Forms.NumericUpDown();
            this.numVirus = new System.Windows.Forms.NumericUpDown();
            this.numMoviendose = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numDemonios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFantasmas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSegundos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTamañoCacharro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPersonas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVirus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMoviendose)).BeginInit();
            this.SuspendLayout();
            // 
            // numDemonios
            // 
            this.numDemonios.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numDemonios.Location = new System.Drawing.Point(105, 208);
            this.numDemonios.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numDemonios.Name = "numDemonios";
            this.numDemonios.Size = new System.Drawing.Size(66, 38);
            this.numDemonios.TabIndex = 19;
            this.numDemonios.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numDemonios.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numFantasmas
            // 
            this.numFantasmas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numFantasmas.Location = new System.Drawing.Point(105, 135);
            this.numFantasmas.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numFantasmas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numFantasmas.Name = "numFantasmas";
            this.numFantasmas.Size = new System.Drawing.Size(66, 38);
            this.numFantasmas.TabIndex = 17;
            this.numFantasmas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numFantasmas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numSegundos
            // 
            this.numSegundos.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numSegundos.Location = new System.Drawing.Point(105, 66);
            this.numSegundos.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numSegundos.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numSegundos.Name = "numSegundos";
            this.numSegundos.Size = new System.Drawing.Size(66, 38);
            this.numSegundos.TabIndex = 15;
            this.numSegundos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numSegundos.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // btnJuego
            // 
            this.btnJuego.Location = new System.Drawing.Point(37, 549);
            this.btnJuego.Name = "btnJuego";
            this.btnJuego.Size = new System.Drawing.Size(142, 74);
            this.btnJuego.TabIndex = 14;
            this.btnJuego.Text = "JUGAR FANTASMAS";
            this.btnJuego.UseVisualStyleBackColor = true;
            this.btnJuego.Click += new System.EventHandler(this.btnJuego_Click);
            // 
            // chkJugarConBonus
            // 
            this.chkJugarConBonus.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkJugarConBonus.AutoSize = true;
            this.chkJugarConBonus.BackColor = System.Drawing.Color.Red;
            this.chkJugarConBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkJugarConBonus.Location = new System.Drawing.Point(105, 275);
            this.chkJugarConBonus.Name = "chkJugarConBonus";
            this.chkJugarConBonus.Size = new System.Drawing.Size(67, 42);
            this.chkJugarConBonus.TabIndex = 21;
            this.chkJugarConBonus.Text = "NO";
            this.chkJugarConBonus.UseVisualStyleBackColor = false;
            this.chkJugarConBonus.CheckedChanged += new System.EventHandler(this.chkJugarConBonus_CheckedChanged);
            // 
            // cmbDificultad
            // 
            this.cmbDificultad.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDificultad.FormattingEnabled = true;
            this.cmbDificultad.Items.AddRange(new object[] {
            "",
            "FACIL",
            "DIFICIL",
            "EXTREMO"});
            this.cmbDificultad.Location = new System.Drawing.Point(12, 12);
            this.cmbDificultad.Name = "cmbDificultad";
            this.cmbDificultad.Size = new System.Drawing.Size(198, 39);
            this.cmbDificultad.TabIndex = 26;
            this.cmbDificultad.SelectedIndexChanged += new System.EventHandler(this.cmbDificultad_SelectedIndexChanged);
            // 
            // chkAñadirTiempo
            // 
            this.chkAñadirTiempo.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkAñadirTiempo.AutoSize = true;
            this.chkAñadirTiempo.BackColor = System.Drawing.Color.Red;
            this.chkAñadirTiempo.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAñadirTiempo.Location = new System.Drawing.Point(104, 344);
            this.chkAñadirTiempo.Name = "chkAñadirTiempo";
            this.chkAñadirTiempo.Size = new System.Drawing.Size(67, 42);
            this.chkAñadirTiempo.TabIndex = 27;
            this.chkAñadirTiempo.Text = "NO";
            this.chkAñadirTiempo.UseVisualStyleBackColor = false;
            this.chkAñadirTiempo.CheckedChanged += new System.EventHandler(this.chkAñadirTiempo_CheckedChanged);
            // 
            // chkUsarMovimiento
            // 
            this.chkUsarMovimiento.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkUsarMovimiento.AutoSize = true;
            this.chkUsarMovimiento.BackColor = System.Drawing.Color.Red;
            this.chkUsarMovimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUsarMovimiento.Location = new System.Drawing.Point(104, 406);
            this.chkUsarMovimiento.Name = "chkUsarMovimiento";
            this.chkUsarMovimiento.Size = new System.Drawing.Size(67, 42);
            this.chkUsarMovimiento.TabIndex = 29;
            this.chkUsarMovimiento.Text = "NO";
            this.chkUsarMovimiento.UseVisualStyleBackColor = false;
            this.chkUsarMovimiento.CheckedChanged += new System.EventHandler(this.chkUsarMovimiento_CheckedChanged);
            // 
            // rbLentos
            // 
            this.rbLentos.AutoSize = true;
            this.rbLentos.Location = new System.Drawing.Point(283, 398);
            this.rbLentos.Name = "rbLentos";
            this.rbLentos.Size = new System.Drawing.Size(52, 17);
            this.rbLentos.TabIndex = 31;
            this.rbLentos.Text = "Lento";
            this.rbLentos.UseVisualStyleBackColor = true;
            // 
            // rbRapidos
            // 
            this.rbRapidos.AutoSize = true;
            this.rbRapidos.Location = new System.Drawing.Point(283, 444);
            this.rbRapidos.Name = "rbRapidos";
            this.rbRapidos.Size = new System.Drawing.Size(59, 17);
            this.rbRapidos.TabIndex = 32;
            this.rbRapidos.Text = "Rapido";
            this.rbRapidos.UseVisualStyleBackColor = true;
            // 
            // rbStandard
            // 
            this.rbStandard.AutoSize = true;
            this.rbStandard.Checked = true;
            this.rbStandard.Location = new System.Drawing.Point(283, 421);
            this.rbStandard.Name = "rbStandard";
            this.rbStandard.Size = new System.Drawing.Size(58, 17);
            this.rbStandard.TabIndex = 33;
            this.rbStandard.TabStop = true;
            this.rbStandard.Text = "Normal";
            this.rbStandard.UseVisualStyleBackColor = true;
            // 
            // chPintura
            // 
            this.chPintura.Appearance = System.Windows.Forms.Appearance.Button;
            this.chPintura.AutoSize = true;
            this.chPintura.BackColor = System.Drawing.Color.Green;
            this.chPintura.Checked = true;
            this.chPintura.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chPintura.Font = new System.Drawing.Font("Microsoft Sans Serif", 21F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chPintura.Location = new System.Drawing.Point(104, 475);
            this.chPintura.Name = "chPintura";
            this.chPintura.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chPintura.Size = new System.Drawing.Size(51, 42);
            this.chPintura.TabIndex = 34;
            this.chPintura.Text = "SI";
            this.chPintura.UseVisualStyleBackColor = false;
            this.chPintura.CheckedChanged += new System.EventHandler(this.chPintura_CheckedChanged);
            // 
            // btnCorona
            // 
            this.btnCorona.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCorona.Location = new System.Drawing.Point(813, 12);
            this.btnCorona.Name = "btnCorona";
            this.btnCorona.Size = new System.Drawing.Size(126, 611);
            this.btnCorona.TabIndex = 36;
            this.btnCorona.Text = "C\r\nO\r\nR\r\nO\r\nN\r\nA\r\n\r\nV\r\nI\r\nR\r\nU\r\nS";
            this.btnCorona.UseVisualStyleBackColor = true;
            this.btnCorona.Click += new System.EventHandler(this.btnCorona_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::CompeticionClicks.Properties.Resources.pintura;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.InitialImage = null;
            this.pictureBox7.Location = new System.Drawing.Point(41, 470);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(50, 50);
            this.pictureBox7.TabIndex = 35;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::CompeticionClicks.Properties.Resources.movement;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.InitialImage = null;
            this.pictureBox6.Location = new System.Drawing.Point(41, 401);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 50);
            this.pictureBox6.TabIndex = 30;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::CompeticionClicks.Properties.Resources.add_time;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.InitialImage = null;
            this.pictureBox5.Location = new System.Drawing.Point(40, 338);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(50, 50);
            this.pictureBox5.TabIndex = 28;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::CompeticionClicks.Properties.Resources.hiclipart_com;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.InitialImage = null;
            this.pictureBox4.Location = new System.Drawing.Point(41, 270);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.TabIndex = 25;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::CompeticionClicks.Properties.Resources.tempotizador;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.InitialImage = null;
            this.pictureBox3.Location = new System.Drawing.Point(41, 61);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(50, 50);
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::CompeticionClicks.Properties.Resources.demonio;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.InitialImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(41, 202);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::CompeticionClicks.Properties.Resources.fantasma;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(41, 128);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // chkEnLinea
            // 
            this.chkEnLinea.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkEnLinea.AutoSize = true;
            this.chkEnLinea.BackColor = System.Drawing.Color.Red;
            this.chkEnLinea.Location = new System.Drawing.Point(189, 406);
            this.chkEnLinea.Name = "chkEnLinea";
            this.chkEnLinea.Size = new System.Drawing.Size(66, 23);
            this.chkEnLinea.TabIndex = 37;
            this.chkEnLinea.Text = "EN LÍNEA";
            this.chkEnLinea.UseVisualStyleBackColor = false;
            this.chkEnLinea.CheckedChanged += new System.EventHandler(this.chkEnLinea_CheckedChanged);
            // 
            // chkAlAzar
            // 
            this.chkAlAzar.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkAlAzar.AutoSize = true;
            this.chkAlAzar.BackColor = System.Drawing.Color.Red;
            this.chkAlAzar.Location = new System.Drawing.Point(189, 432);
            this.chkAlAzar.Name = "chkAlAzar";
            this.chkAlAzar.Size = new System.Drawing.Size(78, 23);
            this.chkAlAzar.TabIndex = 38;
            this.chkAlAzar.Text = "ALEATORIO";
            this.chkAlAzar.UseVisualStyleBackColor = false;
            this.chkAlAzar.CheckedChanged += new System.EventHandler(this.chkAlAzar_CheckedChanged);
            // 
            // numTamañoCacharro
            // 
            this.numTamañoCacharro.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numTamañoCacharro.Location = new System.Drawing.Point(711, 183);
            this.numTamañoCacharro.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numTamañoCacharro.Name = "numTamañoCacharro";
            this.numTamañoCacharro.Size = new System.Drawing.Size(96, 38);
            this.numTamañoCacharro.TabIndex = 39;
            this.numTamañoCacharro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numTamañoCacharro.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // numPersonas
            // 
            this.numPersonas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numPersonas.Location = new System.Drawing.Point(711, 249);
            this.numPersonas.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numPersonas.Name = "numPersonas";
            this.numPersonas.Size = new System.Drawing.Size(96, 38);
            this.numPersonas.TabIndex = 40;
            this.numPersonas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numPersonas.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // numVirus
            // 
            this.numVirus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numVirus.Location = new System.Drawing.Point(711, 312);
            this.numVirus.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numVirus.Name = "numVirus";
            this.numVirus.Size = new System.Drawing.Size(96, 38);
            this.numVirus.TabIndex = 41;
            this.numVirus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numVirus.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numMoviendose
            // 
            this.numMoviendose.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numMoviendose.Location = new System.Drawing.Point(711, 377);
            this.numMoviendose.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numMoviendose.Name = "numMoviendose";
            this.numMoviendose.Size = new System.Drawing.Size(96, 38);
            this.numMoviendose.TabIndex = 42;
            this.numMoviendose.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numMoviendose.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // FrmInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(960, 629);
            this.Controls.Add(this.numMoviendose);
            this.Controls.Add(this.numVirus);
            this.Controls.Add(this.numPersonas);
            this.Controls.Add(this.numTamañoCacharro);
            this.Controls.Add(this.chkAlAzar);
            this.Controls.Add(this.chkEnLinea);
            this.Controls.Add(this.btnCorona);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.chPintura);
            this.Controls.Add(this.rbStandard);
            this.Controls.Add(this.rbRapidos);
            this.Controls.Add(this.rbLentos);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.chkUsarMovimiento);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.chkAñadirTiempo);
            this.Controls.Add(this.cmbDificultad);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.chkJugarConBonus);
            this.Controls.Add(this.numDemonios);
            this.Controls.Add(this.numFantasmas);
            this.Controls.Add(this.numSegundos);
            this.Controls.Add(this.btnJuego);
            this.MinimizeBox = false;
            this.Name = "FrmInicio";
            this.Text = "FrmInicio";
            ((System.ComponentModel.ISupportInitialize)(this.numDemonios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numFantasmas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSegundos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTamañoCacharro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPersonas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVirus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMoviendose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NumericUpDown numDemonios;
        private System.Windows.Forms.NumericUpDown numFantasmas;
        private System.Windows.Forms.NumericUpDown numSegundos;
        private System.Windows.Forms.Button btnJuego;
        private System.Windows.Forms.CheckBox chkJugarConBonus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ComboBox cmbDificultad;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.CheckBox chkAñadirTiempo;
        private System.Windows.Forms.CheckBox chkUsarMovimiento;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.RadioButton rbLentos;
        private System.Windows.Forms.RadioButton rbRapidos;
        private System.Windows.Forms.RadioButton rbStandard;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.CheckBox chPintura;
        private System.Windows.Forms.Button btnCorona;
        private System.Windows.Forms.CheckBox chkEnLinea;
        private System.Windows.Forms.CheckBox chkAlAzar;
        private System.Windows.Forms.NumericUpDown numTamañoCacharro;
        private System.Windows.Forms.NumericUpDown numPersonas;
        private System.Windows.Forms.NumericUpDown numVirus;
        private System.Windows.Forms.NumericUpDown numMoviendose;
    }
}