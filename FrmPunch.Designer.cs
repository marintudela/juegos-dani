﻿namespace CompeticionClicks
{
    partial class FrmPunch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblContadorClicks = new System.Windows.Forms.Label();
            this.temporizador = new System.Windows.Forms.Timer(this.components);
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnClasificaciones = new System.Windows.Forms.Button();
            this.temporizadorBonus = new System.Windows.Forms.Timer(this.components);
            this.temporizadorCrearBonus = new System.Windows.Forms.Timer(this.components);
            this.timerMovimiento = new System.Windows.Forms.Timer(this.components);
            this.btnStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblContadorClicks
            // 
            this.lblContadorClicks.AutoSize = true;
            this.lblContadorClicks.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorClicks.ForeColor = System.Drawing.Color.White;
            this.lblContadorClicks.Location = new System.Drawing.Point(12, 9);
            this.lblContadorClicks.Name = "lblContadorClicks";
            this.lblContadorClicks.Size = new System.Drawing.Size(30, 31);
            this.lblContadorClicks.TabIndex = 1;
            this.lblContadorClicks.Text = "0";
            this.lblContadorClicks.Visible = false;
            // 
            // temporizador
            // 
            this.temporizador.Interval = 1000;
            this.temporizador.Tick += new System.EventHandler(this.temporizador_Tick);
            // 
            // txtNombre
            // 
            this.txtNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtNombre.BackColor = System.Drawing.Color.Black;
            this.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.ForeColor = System.Drawing.Color.Red;
            this.txtNombre.Location = new System.Drawing.Point(276, 60);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(221, 26);
            this.txtNombre.TabIndex = 2;
            this.txtNombre.Text = "NAME";
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNombre.Click += new System.EventHandler(this.txtNombre_Click);
            // 
            // btnClasificaciones
            // 
            this.btnClasificaciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClasificaciones.BackColor = System.Drawing.Color.Black;
            this.btnClasificaciones.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnClasificaciones.ForeColor = System.Drawing.Color.Red;
            this.btnClasificaciones.Location = new System.Drawing.Point(593, 283);
            this.btnClasificaciones.Name = "btnClasificaciones";
            this.btnClasificaciones.Size = new System.Drawing.Size(180, 43);
            this.btnClasificaciones.TabIndex = 3;
            this.btnClasificaciones.Text = "CLASIFICACIONES";
            this.btnClasificaciones.UseVisualStyleBackColor = false;
            this.btnClasificaciones.Click += new System.EventHandler(this.btnClasificaciones_Click);
            // 
            // temporizadorBonus
            // 
            this.temporizadorBonus.Interval = 700;
            this.temporizadorBonus.Tick += new System.EventHandler(this.temporizadorBonus_Tick);
            // 
            // temporizadorCrearBonus
            // 
            this.temporizadorCrearBonus.Interval = 1000;
            this.temporizadorCrearBonus.Tick += new System.EventHandler(this.temporizadorCrearBonus_Tick);
            // 
            // timerMovimiento
            // 
            this.timerMovimiento.Tick += new System.EventHandler(this.timerMovimiento_Tick);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStart.BackColor = System.Drawing.Color.Black;
            this.btnStart.BackgroundImage = global::CompeticionClicks.Properties.Resources.start;
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Location = new System.Drawing.Point(250, 92);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(269, 248);
            this.btnStart.TabIndex = 0;
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // FrmPunch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(785, 338);
            this.Controls.Add(this.btnClasificaciones);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblContadorClicks);
            this.Controls.Add(this.btnStart);
            this.MinimizeBox = false;
            this.Name = "FrmPunch";
            this.Text = "Juego Super Punch DANI";
            this.Click += new System.EventHandler(this.FrmPunch_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label lblContadorClicks;
        private System.Windows.Forms.Timer temporizador;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnClasificaciones;
        private System.Windows.Forms.Timer temporizadorBonus;
        private System.Windows.Forms.Timer temporizadorCrearBonus;
        private System.Windows.Forms.Timer timerMovimiento;
    }
}