﻿namespace CompeticionClicks
{
    partial class FrmClasificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMasNuevos = new System.Windows.Forms.Button();
            this.btnMasRapidos = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.dgvClasificacion = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClasificacion)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMasNuevos
            // 
            this.btnMasNuevos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnMasNuevos.Location = new System.Drawing.Point(76, 481);
            this.btnMasNuevos.Name = "btnMasNuevos";
            this.btnMasNuevos.Size = new System.Drawing.Size(79, 40);
            this.btnMasNuevos.TabIndex = 1;
            this.btnMasNuevos.Text = "MAS NUEVOS";
            this.btnMasNuevos.UseVisualStyleBackColor = true;
            this.btnMasNuevos.Click += new System.EventHandler(this.btnMasNuevos_Click);
            // 
            // btnMasRapidos
            // 
            this.btnMasRapidos.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnMasRapidos.Location = new System.Drawing.Point(312, 481);
            this.btnMasRapidos.Name = "btnMasRapidos";
            this.btnMasRapidos.Size = new System.Drawing.Size(79, 40);
            this.btnMasRapidos.TabIndex = 2;
            this.btnMasRapidos.Text = "MAS PUNTOS";
            this.btnMasRapidos.UseVisualStyleBackColor = true;
            this.btnMasRapidos.Click += new System.EventHandler(this.btnMasRapidos_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Location = new System.Drawing.Point(572, 481);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(79, 40);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Location = new System.Drawing.Point(572, 455);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(79, 20);
            this.txtPassword.TabIndex = 4;
            // 
            // dgvClasificacion
            // 
            this.dgvClasificacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvClasificacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClasificacion.Location = new System.Drawing.Point(8, 9);
            this.dgvClasificacion.Name = "dgvClasificacion";
            this.dgvClasificacion.Size = new System.Drawing.Size(705, 440);
            this.dgvClasificacion.TabIndex = 5;
            // 
            // FrmClasificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 535);
            this.Controls.Add(this.dgvClasificacion);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnMasRapidos);
            this.Controls.Add(this.btnMasNuevos);
            this.Name = "FrmClasificaciones";
            this.Text = "Clasificaciones";
            this.Load += new System.EventHandler(this.FrmClasificaciones_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClasificacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnMasNuevos;
        private System.Windows.Forms.Button btnMasRapidos;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.DataGridView dgvClasificacion;
    }
}