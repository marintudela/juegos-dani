﻿namespace CompeticionClicks
{
    partial class FrmCorona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMovimiento = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSanados = new System.Windows.Forms.Label();
            this.lblMuertos = new System.Windows.Forms.Label();
            this.lblInfectados = new System.Windows.Forms.Label();
            this.lblSanos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timerMovimiento
            // 
            this.timerMovimiento.Tick += new System.EventHandler(this.timerMovimiento_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(632, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Healthy:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(632, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Infected:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Yellow;
            this.label3.Location = new System.Drawing.Point(632, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Deaths:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(632, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Healed:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblSanados
            // 
            this.lblSanados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSanados.AutoSize = true;
            this.lblSanados.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSanados.ForeColor = System.Drawing.Color.Yellow;
            this.lblSanados.Location = new System.Drawing.Point(757, 99);
            this.lblSanados.Name = "lblSanados";
            this.lblSanados.Size = new System.Drawing.Size(25, 25);
            this.lblSanados.TabIndex = 7;
            this.lblSanados.Text = "0";
            this.lblSanados.Click += new System.EventHandler(this.lblSanados_Click);
            // 
            // lblMuertos
            // 
            this.lblMuertos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMuertos.AutoSize = true;
            this.lblMuertos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMuertos.ForeColor = System.Drawing.Color.Yellow;
            this.lblMuertos.Location = new System.Drawing.Point(757, 69);
            this.lblMuertos.Name = "lblMuertos";
            this.lblMuertos.Size = new System.Drawing.Size(25, 25);
            this.lblMuertos.TabIndex = 6;
            this.lblMuertos.Text = "0";
            this.lblMuertos.Click += new System.EventHandler(this.lblMuertos_Click);
            // 
            // lblInfectados
            // 
            this.lblInfectados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInfectados.AutoSize = true;
            this.lblInfectados.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfectados.ForeColor = System.Drawing.Color.Yellow;
            this.lblInfectados.Location = new System.Drawing.Point(757, 39);
            this.lblInfectados.Name = "lblInfectados";
            this.lblInfectados.Size = new System.Drawing.Size(25, 25);
            this.lblInfectados.TabIndex = 5;
            this.lblInfectados.Text = "0";
            this.lblInfectados.Click += new System.EventHandler(this.lblInfectados_Click);
            // 
            // lblSanos
            // 
            this.lblSanos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSanos.AutoSize = true;
            this.lblSanos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSanos.ForeColor = System.Drawing.Color.Yellow;
            this.lblSanos.Location = new System.Drawing.Point(757, 9);
            this.lblSanos.Name = "lblSanos";
            this.lblSanos.Size = new System.Drawing.Size(25, 25);
            this.lblSanos.TabIndex = 4;
            this.lblSanos.Text = "0";
            this.lblSanos.Click += new System.EventHandler(this.lblSanos_Click);
            // 
            // FrmCorona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblSanados);
            this.Controls.Add(this.lblMuertos);
            this.Controls.Add(this.lblInfectados);
            this.Controls.Add(this.lblSanos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCorona";
            this.Text = "CORONA VIRUS";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmCorona_FormClosing);
            this.Shown += new System.EventHandler(this.FrmCorona_Shown);
            this.Click += new System.EventHandler(this.FrmCorona_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerMovimiento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSanados;
        private System.Windows.Forms.Label lblMuertos;
        private System.Windows.Forms.Label lblInfectados;
        private System.Windows.Forms.Label lblSanos;
    }
}