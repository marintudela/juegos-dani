﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompeticionClicks
{
    public partial class FrmCorona : Form
    {
        Random PosicionInicialRandom = new Random();
        string[] Edades = new string[] { DatosObjeto.Edades.BABY.ToString(), DatosObjeto.Edades.ADULTO.ToString(), DatosObjeto.Edades.ANCIANO.ToString() };

        Dictionary<string, int> PosibilidadMorir = new Dictionary<string, int>();
        Configuracion Configu { get; set; }

        int[,] Pantalla { get; set; }


        public FrmCorona(Configuracion Config)
        {
            InitializeComponent();
            Configu = Config;
            PosibilidadMorir.Add(Edades[0], 1);
            PosibilidadMorir.Add(Edades[1], 20);
            PosibilidadMorir.Add(Edades[2], 80);

            

        }

        private void FrmCorona_Shown(object sender, EventArgs e)
        {
            //Creamos una matriz de anchura por altura de la pantalla (int de pixels)
            Pantalla = new int[Width - 40, Height - 40];
            Iniciar();
        }

        private void Iniciar()
        {
            Configu.Sanos = Configu.CantidadDePersonas;
            CrearCacharros(Configu.CantidadDePersonas, Utiles.TipoBoton.Persona);
            CrearCacharros(Configu.CantidadDeVirus, Utiles.TipoBoton.Virus);
            CrearCacharros(1, Utiles.TipoBoton.Hospital);
            ActualizarContadores();
            timerMovimiento.Start();

            
        }

        int IndicadorDepuracion = 0;

        private void CrearCacharros(int Cantidad, Utiles.TipoBoton Tipo)
        {
            for (int i = 0; i < Cantidad; i++)
            {
                IndicadorDepuracion++;

                DatosObjeto datos = new DatosObjeto();
                int eleccion = 1;
                datos.Edad = Edades[eleccion];
                datos.Infectado = false;
                datos.Tipo = Tipo;
                //Esto genera una clave alfanumérica unica en cada uno:
                datos.Guid = Guid.NewGuid().ToString();
                datos.Id = i + 1;

                //Sacamos la edad random para saber que tipo de persona creamos, están todas en el array Edades
                Button persona = new Button();
                Application.DoEvents();
                persona.FlatStyle = FlatStyle.Flat;
                persona.Size = new Size(Configu.TamañoCacharro, Configu.TamañoCacharro);
                persona.Tag = datos;
                if (!Utiles.PosicionLibreMatriz(Pantalla,this, persona, Configu))
                {
                    persona.Dispose();
                }
                else
                {
                    persona.BackgroundImage = datos.Tipo == Utiles.TipoBoton.Virus ? Properties.Resources.virus
                                            : datos.Tipo == Utiles.TipoBoton.Hospital ? Properties.Resources.curar
                                            : datos.Edad == Edades[0] ? Properties.Resources.baby
                                            : datos.Edad == Edades[1] ? Properties.Resources.adult
                                            : Properties.Resources.old;
                    persona.BackgroundImageLayout = ImageLayout.Zoom;
                    ((DatosObjeto)persona.Tag).TiempoInfectado = new Timer();
                    ((DatosObjeto)persona.Tag).TiempoInfectado.Interval = 10000;
                    ((DatosObjeto)persona.Tag).TiempoInfectado.Tick += new System.EventHandler(this.tiempoInfectado_Tick);
                    ((DatosObjeto)persona.Tag).TiempoInfectado.Tag = datos;

                    if (Tipo == Utiles.TipoBoton.Hospital)
                    {
                        datos.TiempoHospitalSaturado = new Timer();
                        datos.SegundosCuracionHospital = 4;
                        datos.TiempoHospitalSaturado.Interval = 1000;
                        datos.HospitalDisponible = true;
                        ((DatosObjeto)persona.Tag).TiempoHospitalSaturado.Tick += new System.EventHandler(this.tiempoHospital_Tick);
                        ((DatosObjeto)persona.Tag).TiempoHospitalSaturado.Tag = datos;
                        persona.Size = new Size(100, 100);
                    }
                    if (i > Configu.Moviendose)
                    {//Ha sobrepasado el num y estos ya no se van a mover:
                        datos.DireccionAnterior = new Point(0, 0);
                    }
                    else if (datos.Tipo == Utiles.TipoBoton.Persona)
                    {
                        int movimiento = Configu.Speed == Configuracion.Velocidad.Lento ? 10
                          : Configu.Speed == Configuracion.Velocidad.Normal ? 50 : 100;
                        Point direccion = datos.DireccionAnterior;
                        direccion.X = Utiles.Aleatorio.Next(-movimiento, movimiento);
                        direccion.Y = Utiles.Aleatorio.Next(-movimiento, movimiento);
                        datos.DireccionAnterior = direccion;
                    }

                    //persona.Text = IndicadorDepuracion.ToString(); //DPUERACION
                    //persona.Font = new Font("Arial", 24); //DEPURACION
                    Controls.Add(persona);

                }
                Application.DoEvents();
            }
        }

        public void ActualizarContadores()
        {
            lblSanados.Text = Configu.Sanados.ToString();
            lblSanos.Text = Configu.Sanos.ToString();
            lblMuertos.Text = Configu.Muertos.ToString();
            lblInfectados.Text = Configu.Infectados.ToString();
        }

        private void tiempoHospital_Tick(object timer, EventArgs e)
        {
            string guidTimer = ((DatosObjeto)((Timer)timer).Tag).Guid;
            Button botonHospital = Controls.OfType<Button>().Where(w => w.Tag != null && ((DatosObjeto)w.Tag).Guid == guidTimer).First();

            int segActuales = Convert.ToInt32(botonHospital.Text);
            botonHospital.Text = (segActuales--).ToString();

            if(botonHospital.Text == "0")
            {
                ((DatosObjeto)((Button)botonHospital).Tag).HospitalDisponible = true;
                ((DatosObjeto)((Button)botonHospital).Tag).TiempoHospitalSaturado.Stop();

            }


            ActualizarContadores();

        }

        private void tiempoInfectado_Tick(object timer, EventArgs e)
        {
            if (!Visible) return;
            int porcentajeMorir = PosibilidadMorir[((DatosObjeto)((Timer)timer).Tag).Edad];
            int suerte = Utiles.Aleatorio.Next(1, 100);

            //Encontramos el boton relaccionado con este TIMER (en esta funcion el object que la llama no es el botón en sí, es el timer que hemos metido en la tag dentro de la clase objeto:
            //Lo encontramos porque le hemos metido el mismo GUID en las dos TAG, en la del timer y la del boton.
            string guidTimer = ((DatosObjeto)((Timer)timer).Tag).Guid;
            Button botonAfectado = Controls.OfType<Button>().Where(w => w.Tag != null && ((DatosObjeto)w.Tag).Guid == guidTimer).First();

            if (suerte < porcentajeMorir)
            {//Muere:
                ((DatosObjeto)botonAfectado.Tag).Tipo = Utiles.TipoBoton.Muerto;
                ((DatosObjeto)botonAfectado.Tag).Infectado = true;
                ((DatosObjeto)botonAfectado.Tag).TiempoInfectado.Stop();
                botonAfectado.BackColor = Color.Black;
                botonAfectado.BackgroundImage = Properties.Resources.muerto;
                Configu.Muertos++;
                Configu.Infectados--;
            }
            else
            {//Sana:
                botonAfectado.BackColor = Color.Black;
                ((DatosObjeto)botonAfectado.Tag).Infectado = false;
                ((DatosObjeto)botonAfectado.Tag).TiempoInfectado.Stop();
                Configu.Sanados++;
                Configu.Infectados--;
                Configu.Sanos++;
            }
            ActualizarContadores();
        }

        private void timerMovimiento_Tick(object sender, EventArgs e)
        {
            Application.DoEvents();
            timerMovimiento.Enabled = false;
            Utiles.MoverseLinealmentePorLaPantalla(this, Configu);
            timerMovimiento.Enabled = true;
            Application.DoEvents();

        }

        private void FrmCorona_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Esta función elimina todos los controles al cerrar el formulario, si lo cerrabamos de la X, en el intervalo de tiempo entre que se cierra de verdad y el click, se ejecutaban algunos ticks de los timer y ya no encontrata el boton asociado.
            foreach(Control control in Controls)
            {
                control.Dispose();
            }
        }

        private void FrmCorona_Click(object sender, EventArgs e)
        {
            
            var existentes = Controls.OfType<Button>().Where(w => ((DatosObjeto)w.Tag).Tipo == Utiles.TipoBoton.Persona && ((DatosObjeto)w.Tag).Infectado == false).ToList();
            Utiles.Infectar(existentes.First(), Configu);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblMuertos_Click(object sender, EventArgs e)
        {

        }

        private void lblInfectados_Click(object sender, EventArgs e)
        {

        }

        private void lblSanos_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblSanados_Click(object sender, EventArgs e)
        {

        }
    }

}
