﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompeticionClicks
{
    public class Utiles
    {
        public enum TipoBoton { Todos, Fantasma, Diablo, Bonus, SumarTiempo, Persona, Virus, Muerto, Hospital }
        public static Random Aleatorio = new Random();
        static int MargenesBichos = 20;

        public static string RutaFichero = @"C:\JuegoDani\clasificacion.csv";

        /// <summary>
        /// Enumeracion de sonidos para elegirlos facilmente de los archivos de recursos
        /// </summary>
        public enum TiposSonido { Felicidades, Aplausos, EresUnManta, BeepCorto, Booooo, SonBonus }


        public static async void ReproducirSonido(TiposSonido Sonido)
        {
            await Task.Run(() => ReproducirSonidoNoAsincrono(Sonido));
        }

        /// <summary>
        /// Reproduce el sonido correspondiente que se la pasa en la enumeracion
        /// </summary>
        /// <param name="Tipo">Enumeracion que sirve para elegir un sonido u otro</param>
        private static void ReproducirSonidoNoAsincrono(TiposSonido Tipo)
        {
            //Declaramos un reproductor de sonidos y luego le cargamos con Stream el archivo que vamos a reproducir
            SoundPlayer son = new SoundPlayer();
            if (Tipo == TiposSonido.Aplausos)
            {
                son.Stream = CompeticionClicks.Properties.Resources.crowd5;
            }
            else if (Tipo == TiposSonido.Felicidades)
            {
                son.Stream = CompeticionClicks.Properties.Resources.cuco;
            }
            else if (Tipo == TiposSonido.Booooo)
            {
                son.Stream = CompeticionClicks.Properties.Resources.beep_10;
            }
            else if (Tipo == TiposSonido.BeepCorto)
            {
                son.Stream = CompeticionClicks.Properties.Resources.beep_07;
            }
            else if (Tipo == TiposSonido.SonBonus)
            {
                son.Stream = CompeticionClicks.Properties.Resources.Bonus;
            }

            else
            {
                son.Stream = CompeticionClicks.Properties.Resources.alarma;
            }

            son.Play();
        }


        public static void LimpiarTextosFormulario(Form Formulario, bool BorrarNulos)
        {
            if(BorrarNulos)
            {
                foreach (TextBox texto in Formulario.Controls.OfType<TextBox>())
                {
                    texto.Text = "";
                }
            }
            else
            {
                foreach (TextBox texto in Formulario.Controls.OfType<TextBox>().Where(w => w.Tag != null))
                {
                    texto.Text = "";
                }
            }
        }

        public static bool PosicionLibre(Form Formulario, Button Boton, Configuracion Config)
        {
            Point movimiento = new Point(0, 0);

            int reintentos = 10;

            do
            {
                int locationX = Aleatorio.Next(40, Formulario.Width - 40);
                int locationY = Aleatorio.Next(40, Formulario.Height - 40);
                Boton.Location = new Point(locationX, locationY);
                reintentos--;
                if (reintentos == 0)
                {
                    return false;
                }
            }
            while (SeChoca(Formulario, Boton, movimiento, Config, false));

            return true;

        }

        public static bool PosicionLibreMatriz(int[,] Pantalla, Form Formulario, Button Boton, Configuracion Config)
        {
            Point movimiento = new Point(0, 0);

            int reintentos = 10;

            do
            {
                int locationX = Aleatorio.Next(40, Formulario.Width - 40);
                int locationY = Aleatorio.Next(40, Formulario.Height - 40);
                Boton.Location = new Point(locationX, locationY);
                reintentos--;
                if (reintentos == 0)
                {
                    return false;
                }
            }
            while (SeSolapaMatriz(Pantalla, Formulario, Boton, movimiento, Config, false));


            for(int x = Boton.Location.X; x < Boton.Location.X + Boton.Width - 1; x++)
            {
                for(int y = Boton.Location.Y; y < Boton.Location.Y + Boton.Height - 1; y++)
                {
                    Pantalla[x, y] = ((DatosObjeto)Boton.Tag).Id;


                }
            }


            return true;

        }

        public static void NosesaledelaPantallaenMovimiento(Form formulario, Configuracion Config)
        {
            int movimiento =  Config.Speed == Configuracion.Velocidad.Lento ? Config.TamañoCacharro / 3
                          : Config.Speed == Configuracion.Velocidad.Normal ? Config.TamañoCacharro / 2 : Convert.ToInt32(Config.TamañoCacharro * 1.5);

            foreach (Button bicho in formulario.Controls.OfType<Button>().Where(w => w.Tag != null))
            {
                int x = Utiles.Aleatorio.Next(-movimiento, movimiento);
                int y = Utiles.Aleatorio.Next(-movimiento, movimiento);
                //Que no se salga por la DERECHA:
                while (bicho.Location.X + bicho.Size.Width + x > formulario.Size.Width)
                {
                    x = Utiles.Aleatorio.Next(-movimiento, movimiento);
                }
                //Que no se salga por la IZQUIERDA:
                while (bicho.Location.X + x < 0)
                {
                    x = Utiles.Aleatorio.Next(-movimiento, movimiento);
                }
                //Que no se salga por la ABAJO:
                while (bicho.Location.Y + bicho.Size.Height + y > formulario.Size.Height - 30)
                {
                    y = Utiles.Aleatorio.Next(-movimiento, movimiento);
                }
                //Que no se salga por la ARRIBA:
                while (bicho.Location.Y + y < 0)
                {
                    y = Utiles.Aleatorio.Next(-movimiento, movimiento);
                }

                bicho.Location = new Point(bicho.Location.X + x, bicho.Location.Y + y);




            }

        }

        private static bool SuperPuestos(Button BotonMueve, Button BotonQuieto, Point Movimiento)
        {
            bool dentro1 = (BotonMueve.Left + Movimiento.X >= BotonQuieto.Left && BotonMueve.Left + Movimiento.X <= BotonQuieto.Right) 
                        || (BotonMueve.Right + Movimiento.X >= BotonQuieto.Left && BotonMueve.Right + Movimiento.X <= BotonQuieto.Right);
            bool dentro2 = (BotonMueve.Top + Movimiento.Y >= BotonQuieto.Top && BotonMueve.Top + Movimiento.Y <= BotonQuieto.Bottom) 
                        || (BotonMueve.Bottom + Movimiento.Y >= BotonQuieto.Top && BotonMueve.Bottom + Movimiento.Y <= BotonQuieto.Bottom);
            return dentro1 && dentro2;

        }

        private static bool ComprobarSePegaConOtro(Form Formulario, Button Nuevo, Point Movimiento)
        {
            bool superpuesto = false;

            foreach (Button viejo in Formulario.Controls.OfType<Button>().Where(w => w.Tag != null /*&& ((DatosObjeto)w.Tag).Guid != GuidNoComprobar*/))
            {
                if (Nuevo != viejo)
                {
                    superpuesto = SuperPuestos(Nuevo, viejo, Movimiento);
                }

                //Si se pega con uno de los botones existentes:
                if (superpuesto)
                {
                    Console.WriteLine("EL BOTON " + Nuevo.Text + " SE PEGA CON EL BOTON " + viejo.Text);
                    break;
                }
            }
            return superpuesto;
        }


        public static bool ComprobarSiUnBotonoNoEsCoincidenteConOtro(Form Formulario, Button Nuevo, Configuracion Config, Point Movimiento)
        {
            bool superpuesto = true;

            //Para cuando no hay botones todavia en el formulario:
            if (Formulario.Controls.OfType<Button>().Where(w => w.Tag != null /*&& ((DatosObjeto)w.Tag).Guid != GuidNoComprobar*/).Count() == 0)
            {
                return false;
            }

            foreach (Button viejo in Formulario.Controls.OfType<Button>().Where(w => w.Tag != null /*&& ((DatosObjeto)w.Tag).Guid != GuidNoComprobar*/))
            {
                superpuesto = SuperPuestos(Nuevo, viejo, Movimiento);
                //Estas acciones solo las hace en el coronavirus:
                if (superpuesto && Config.Juego == Configuracion.TipoJuego.CoronaVirus && Nuevo != viejo)
                {
                    if (((DatosObjeto)viejo.Tag).Tipo == TipoBoton.Hospital)
                    { //Aqui han tocado el HOSPITAL (es viejo)
                        if (((DatosObjeto)Nuevo.Tag).Infectado && ((DatosObjeto)viejo.Tag).HospitalDisponible)
                        {
                            ((DatosObjeto)Nuevo.Tag).Infectado = false;
                            ((DatosObjeto)viejo.Tag).HospitalDisponible = false;
                            viejo.Text = (((DatosObjeto)viejo.Tag).SegundosCuracionHospital / 1000).ToString();
                            ((DatosObjeto)viejo.Tag).TiempoHospitalSaturado.Start();
                            Config.Infectados--;
                            Config.Sanos++;
                        }

                    }
                    else
                    {
                        //Aqui SE INFECTAN SI TOCA:
                        bool viejoInfecta = ((DatosObjeto)viejo.Tag).Infectado || ((DatosObjeto)viejo.Tag).Tipo == TipoBoton.Virus;
                        bool nuevoInfecta = ((DatosObjeto)Nuevo.Tag).Infectado || ((DatosObjeto)Nuevo.Tag).Tipo == TipoBoton.Virus;
                        if (viejoInfecta)
                        {
                            Infectar(Nuevo, Config);
                        }
                        if (nuevoInfecta)
                        {
                            Infectar(viejo, Config);
                        }
                    }

                    ((FrmCorona)Formulario).ActualizarContadores();

                    //Si se pega con uno de los botones existentes:
                    Console.WriteLine("EL BOTON " + Nuevo.Text + " SE PEGA CON EL BOTON " + viejo.Text);

                    return superpuesto;
                }                
            }
            
            return superpuesto;
        }

        public static void Infectar(Button BotonAInfectar, Configuracion Config)
        {
            if(((DatosObjeto)BotonAInfectar.Tag).Tipo == TipoBoton.Virus || ((DatosObjeto)BotonAInfectar.Tag).Tipo == TipoBoton.Muerto)
            {//Estos tipos de boton no se infectan
                return;
            }
            if (!((DatosObjeto)BotonAInfectar.Tag).Infectado)
            {
                Config.Infectados++;
                Config.Sanos--;
            }
            ((DatosObjeto)BotonAInfectar.Tag).Infectado = true;
            BotonAInfectar.BackColor = Color.Red;
            ((DatosObjeto)BotonAInfectar.Tag).TiempoInfectado.Start();
            
        }

        public static bool ComprobarSiUnCuadradoEstaEnElFormulario(Form Formulario, Button Boton, Point Movimiento)
        {
            bool dentro = true;

            Point[] esquinas = EsquinasDeUnBoton(Boton, Movimiento);


            for (int i = 0; i < esquinas.Count(); i++)
            {
                bool dentroX = (esquinas[i].X >= 0 && esquinas[i].X <= Formulario.Width);
                bool dentroY = (esquinas[i].Y >= 0 && esquinas[i].Y <= Formulario.Height);
                dentro = dentroX && dentroY;

                if(!dentro)
                {
                    break;
                }
            }

            return dentro;
        }

        public static bool SeSolapaMatriz(int[,] Pantalla, Form formulario, Button Boton, Point Movimiento, Configuracion Config, bool ConAccion)
        {

            bool dentroFormulario = ComprobarSiUnCuadradoEstaEnElFormulario(formulario, Boton, Movimiento);
            bool sePegaConOtro= false;

            int x = Boton.Location.X + Movimiento.X; int y = 0;
            while ((x < Boton.Location.X + Movimiento.X + Boton.Width) && (x < 600))
            {
                y = Boton.Location.Y + Movimiento.Y;
                while ((y < Boton.Location.Y + Movimiento.Y + Boton.Height) && (y < 600))
                {
                        if (Pantalla[x, y] != 0 && Pantalla[x, y] != ((DatosObjeto)Boton.Tag).Id )
                            sePegaConOtro = true;
                        else
                        { }
                    y++;
                }
                x++;
            }
            return sePegaConOtro;

            if (ConAccion)
            {
                sePegaConOtro = ComprobarSiUnBotonoNoEsCoincidenteConOtro(formulario, Boton, Config, Movimiento);
            }
            else
            {
                sePegaConOtro = ComprobarSePegaConOtro(formulario, Boton, new Point(0, 0));
            }

            Console.WriteLine("BOTON " + Boton.Text + " DENTRO DE FORMULARIO: " + dentroFormulario + " SE PEGA CON OTRO: " + sePegaConOtro);

            return !(dentroFormulario && !sePegaConOtro);
        }

        public static bool SeChoca(Form formulario, Button Boton,Point Movimiento, Configuracion Config, bool ConAccion)
        {
           //var existentes = formulario.Controls.OfType<Button>().Where(w => ((DatosObjeto)w.Tag).Guid == GuidBichoComprobando).ToList();
           //string numero = existentes != null && existentes.Count() > 0 ? existentes.First().Text : "";

            
            bool dentroFormulario = ComprobarSiUnCuadradoEstaEnElFormulario(formulario, Boton, Movimiento);
            bool sePegaConOtro;
            if (ConAccion)
            {
                sePegaConOtro = ComprobarSiUnBotonoNoEsCoincidenteConOtro(formulario, Boton, Config, Movimiento);
            }
            else
            {
                sePegaConOtro = ComprobarSePegaConOtro(formulario, Boton, new Point(0,0));
            }

            Console.WriteLine("BOTON " + Boton.Text + " DENTRO DE FORMULARIO: " + dentroFormulario + " SE PEGA CON OTRO: " + sePegaConOtro);

            return !(dentroFormulario && !sePegaConOtro);
        }

        private static Point[] EsquinasDeUnBoton(Button bicho, Point direccion)
        {
            Point posicionArribaIzquierda = new Point(bicho.Location.X + direccion.X, bicho.Location.Y + direccion.Y);
            Point posicionAbajoDerecha = new Point(bicho.Location.X + bicho.Width + direccion.X, bicho.Location.Y + direccion.Y + bicho.Height + 40);
            Point posicionArribaDerecha = new Point(bicho.Location.X + bicho.Width + direccion.X, bicho.Location.Y + direccion.Y);
            Point posicionAbajoIzquierda = new Point(bicho.Location.X + direccion.X, bicho.Location.Y + bicho.Height + direccion.Y + 40);
            return new Point[] { posicionArribaIzquierda, posicionAbajoDerecha, posicionAbajoIzquierda, posicionArribaDerecha };
        }



        public static void MoverseLinealmentePorLaPantalla(Form formulario, Configuracion Config)
        {            
            int movimiento = Config.Speed == Configuracion.Velocidad.Lento ? Config.TamañoCacharro / 3
                          : Config.Speed == Configuracion.Velocidad.Normal ? Config.TamañoCacharro / 2 : Convert.ToInt32(Config.TamañoCacharro * 1.5);


            foreach (Button bicho in formulario.Controls.OfType<Button>().Where(w => w.Tag != null))
            {
                Point direccion = ((DatosObjeto)bicho.Tag).DireccionAnterior;
                if (((DatosObjeto)bicho.Tag).Tipo == TipoBoton.Virus || ((DatosObjeto)bicho.Tag).Tipo == TipoBoton.Muerto || direccion == null || (direccion.X == 0 && direccion.Y == 0))
                {
                    continue;
                }

                //Si con el nuevo movimiento no se sale de la pantalla, lo seguimos usando:

                //Si es la primera no tendrá dirección, se la ponemos random:               
                //Comprobar si cualquiera de los 4 puntos, sumandole la nueva dirección va a estar dentro del formulario
                //Si no lo está hay que buscar otra dirección con un while hasta que el bicho vuelva a estar en el formulario:   

                long reintentos = 1000;

                while (SeChoca(formulario,bicho,direccion,Config, true))
                {
                    //Si entra en este while, lo que hace es cambiar la dirección del movimiento porque choca con borde del form
                    direccion.X = Utiles.Aleatorio.Next(-movimiento, movimiento);
                    direccion.Y = Utiles.Aleatorio.Next(-movimiento, movimiento);
                    ((DatosObjeto)bicho.Tag).DireccionAnterior = direccion;
                    reintentos--;
                    if(reintentos<=0)
                    {
                        direccion = new Point(0, 0);
                        break;
                    }
                }

                bicho.Location = new Point(bicho.Location.X + direccion.X, bicho.Location.Y + direccion.Y);
                Application.DoEvents();
            }

        }

    }
}
