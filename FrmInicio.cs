﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CompeticionClicks.Configuracion;

namespace CompeticionClicks
{
    public partial class FrmInicio : Form
    {
        Configuracion ConfigUsuario { get; set; }
        public FrmInicio()
        {
            InitializeComponent();
            PonerValoresIniciales();
        }

        private void RellenarConfiguracion()
        {
            Velocidad Vel = rbLentos.Checked ? Velocidad.Lento : rbStandard.Checked ? Velocidad.Normal : Velocidad.Rapido;

            ConfigUsuario = new Configuracion()
            {
                SegundosIniciales = Convert.ToInt32(numSegundos.Value),
                SegundosJuego = Convert.ToInt32(numSegundos.Value),
                UsarBonus = chkJugarConBonus.Checked,
                UsarTiemposExtra = chkAñadirTiempo.Checked,
                BichosBuenos = Convert.ToInt32(numFantasmas.Value),
                BichosMalos = Convert.ToInt32(numDemonios.Value),
                UsarMovimiento = chkUsarMovimiento.Checked,
                Speed = Vel,
                Pintura = chPintura.Checked,
                Dificultad = Convert.ToInt32(cmbDificultad.SelectedIndex),
                IralAzar = chkAlAzar.Checked,
                IrenLinea = chkEnLinea.Checked,
                TamañoCacharro = Convert.ToInt32(numTamañoCacharro.Value),
                CantidadDePersonas = Convert.ToInt32(numPersonas.Value),
                CantidadDeVirus = Convert.ToInt32(numVirus.Value),
                Moviendose = Convert.ToInt32(numMoviendose.Value)
             
            };
        }
        private void btnJuego_Click(object sender, EventArgs e)
        {
            RellenarConfiguracion();
            ConfigUsuario.Juego = TipoJuego.Clicks;
            FrmPunch juego = new FrmPunch(ConfigUsuario);
            juego.WindowState = FormWindowState.Maximized;
            juego.Size = new Size(Screen.AllScreens[0].WorkingArea.Width, Screen.AllScreens[0].WorkingArea.Height);
            juego.MinimumSize = new Size(juego.Width, juego.Height);
            juego.Show();
        }
        private void PonerValoresIniciales()
        {
            numDemonios.Value = 1;
            numFantasmas.Value = 1;
            numSegundos.Value = 10;
            chkJugarConBonus.Checked = false;
            chkAñadirTiempo.Checked = false;
            chkUsarMovimiento.Checked = false;
            chPintura.Checked = true;
            rbStandard.Checked = true;
            chkAlAzar.Checked = false;
            chkEnLinea.Checked = false;
        }
        private void cmbDificultad_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbDificultad.SelectedIndex == 0)
            {
                PonerValoresIniciales();
            }
            else if(cmbDificultad.SelectedIndex == 1)
            {
                numSegundos.Value = 15;
                numFantasmas.Value = 3;               
                numDemonios.Value = 0;
                chkJugarConBonus.Checked = false;
                chkAñadirTiempo.Checked = true;
                chkUsarMovimiento.Checked = true;
                chPintura.Checked = false;
                rbLentos.Checked = true;
                chkAlAzar.Checked = false;
                chkEnLinea.Checked = true;


            }
            else if (cmbDificultad.SelectedIndex == 2)
            {
                numSegundos.Value = 10;
                numFantasmas.Value = 1;
                numDemonios.Value = 3;
                chkJugarConBonus.Checked = true;
                chkAñadirTiempo.Checked = false;
                chkUsarMovimiento.Checked = true;
                chPintura.Checked = true;
                rbStandard.Checked = true;
                chkAlAzar.Checked = true;
                chkEnLinea.Checked = false;
            }
            else
            {
                numSegundos.Value = 5;
                numFantasmas.Value = 2;
                numDemonios.Value = 5;
                chkJugarConBonus.Checked = false;
                chkAñadirTiempo.Checked = false;
                chkUsarMovimiento.Checked = true;
                chPintura.Checked = true;
                rbRapidos.Checked = true;
                chkAlAzar.Checked = false;
                chkEnLinea.Checked = true;
            }
        }

        private void chkJugarConBonus_CheckedChanged(object sender, EventArgs e)
        {
            chkJugarConBonus.Text = chkJugarConBonus.Checked ? "SI" : "NO";
            chkJugarConBonus.BackColor = chkJugarConBonus.Checked ? Color.Green : Color.Red;
            if (chkJugarConBonus.Checked) chkAñadirTiempo.Checked = false;
        }

        private void chkAñadirTiempo_CheckedChanged(object sender, EventArgs e)
        {
            chkAñadirTiempo.Text = chkAñadirTiempo.Checked ? "SI" : "NO";
            chkAñadirTiempo.BackColor = chkAñadirTiempo.Checked ? Color.Green : Color.Red;
            if (chkAñadirTiempo.Checked) chkJugarConBonus.Checked = false;
            
        }

        private void chkUsarMovimiento_CheckedChanged(object sender, EventArgs e)
        {
            chkUsarMovimiento.Text = chkUsarMovimiento.Checked ? "SI" : "NO";
            chkUsarMovimiento.BackColor = chkUsarMovimiento.Checked ? Color.Green : Color.Red;
            if (chkUsarMovimiento.Checked) chkEnLinea.Checked = true;
            else
            {
                chkEnLinea.Checked = false;
                chkAlAzar.Checked = false;
            }
        }

        private void chPintura_CheckedChanged(object sender, EventArgs e)
        {
            chPintura.Text = chPintura.Checked ? "SI" : "NO";
            chPintura.BackColor = chPintura.Checked ? Color.Green : Color.Red;
        }

        private void btnCorona_Click(object sender, EventArgs e)
        {
            RellenarConfiguracion();
            ConfigUsuario.Juego = TipoJuego.CoronaVirus;
            FrmCorona corona = new FrmCorona(ConfigUsuario);
            corona.BackColor = Color.Black;
            corona.WindowState = FormWindowState.Maximized;
            corona.Size = new Size(Screen.AllScreens[0].WorkingArea.Width, Screen.AllScreens[0].WorkingArea.Height);
            corona.MinimumSize = new Size(corona.Width, corona.Height);
            corona.Show();
        }

        private void chkEnLinea_CheckedChanged(object sender, EventArgs e)
        {
            chkEnLinea.BackColor = chkEnLinea.Checked ? Color.Green : Color.Red;
            if (chkEnLinea.Checked) chkAlAzar.Checked = false;

        }

        private void chkAlAzar_CheckedChanged(object sender, EventArgs e)
        {
            chkAlAzar.BackColor = chkAlAzar.Checked ? Color.Green : Color.Red;
            if (chkAlAzar.Checked) chkEnLinea.Checked = false;
        }
    }
}
