﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompeticionClicks
{
    public class Configuracion
    {
        public enum TipoJuego { Clicks, CoronaVirus }
        public TipoJuego Juego { get; set; }
        public enum Velocidad { Lento, Normal, Rapido }
        public int SegundosJuego { get; set; }
        public int SegundosIniciales { get; set; }
        public int BichosMalos { get; set; }
        public int BichosBuenos { get; set; }
        public bool UsarBonus { get; set; }
        public bool UsarTiemposExtra { get; set; }
        public bool UsarMovimiento { get; set; }
        public Velocidad Speed { get; set; }
        public bool Pintura { get; set; }
        public int Dificultad { get; set; }
        public bool IrenLinea { get; set; }
        public bool IralAzar { get; set; }

        public int Sanos = 0;
        public int Sanados = 0;
        public int Infectados = 0;
        public int Muertos = 0;
        public int TamañoCacharro { get; set; }
        public int CantidadDePersonas { get; set; }
        public int CantidadDeVirus { get; set; }
        public int Moviendose { get; set; }

    }
}
