﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CompeticionClicks.Properties;

namespace CompeticionClicks
{
    public partial class FrmPunch : Form
    {
        int TamañoMaximo = 200;
        int TamañoMinimo = 80;
        int Clicks;
        double PuntosObtenidos;       
        Configuracion Configuracion { get; set; }
        

        public FrmPunch(Configuracion Config)
        {
            InitializeComponent();
            Configuracion = Config;
        }

        private void ActualizarSegundos()
        {
            Text = "Juego Super Punch DANI " + Configuracion.SegundosJuego.ToString() + " seg.";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "" || txtNombre.Text == "NAME")
            {
                int num1 = (int)MessageBox.Show("Tienes que decir como te llamas, tonto", "Sin nombre", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                
                if (txtNombre.Text == "DIEGO")
                {
                    int num2 = (int)MessageBox.Show("Vas a perder", "Jugador chungo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                if (txtNombre.Text == "ISABELINES")
                {
                    int num3 = (int)MessageBox.Show("Cada vez que hagas click en el Fantasma te va a sumar dos clicks", "Tienes Ventaja", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                if (Configuracion.UsarBonus || Configuracion.UsarTiemposExtra)
                {
                    temporizadorCrearBonus.Start();
                }
                if(Configuracion.UsarMovimiento)
                {
                    timerMovimiento.Start();
                }
                Clicks = 0;
                txtNombre.Visible = false;
                btnClasificaciones.Visible = false;
                btnStart.Visible = false;
                lblContadorClicks.Visible = false;
                ActualizarSegundos();
                temporizador.Start();
                CrearBicho(Configuracion.BichosBuenos, Utiles.TipoBoton.Fantasma);
                CrearBicho(Configuracion.BichosMalos, Utiles.TipoBoton.Diablo);
            }
        }

        
        
        private void CrearBicho(int Cantidad, Utiles.TipoBoton Tipo)
        {
            for (int index = 0; index < Cantidad; ++index)
            {
                DatosObjeto datos = new DatosObjeto();
                datos.Tipo = Tipo;

                Button button = new Button();
                button.BackgroundImageLayout = ImageLayout.Zoom;
                button.Click += new EventHandler(clickBicho_Click);
                button.BackColor = Color.Black;
                button.FlatStyle = FlatStyle.Flat;

                //Esto genera una clave alfanumérica unica en cada uno:
                datos.Guid = Guid.NewGuid().ToString();

                button.Name = "btn" + Tipo.ToString();
                button.Parent = (Control)this;
                button.Visible = true;
                int tamaño = Utiles.Aleatorio.Next(TamañoMinimo, TamañoMaximo);
                button.Tag = datos;

                if (!Utiles.PosicionLibre(this, button, Configuracion))
                {
                    button.Dispose();
                }
                else
                {
                    if (Tipo == Utiles.TipoBoton.Fantasma)
                    {
                        if (txtNombre.Text == "DIEGO")
                        {
                            button.Size = new Size(tamaño, tamaño);
                            button.BackgroundImage = Resources.Diego;
                            button.SendToBack();
                        }
                        else if (txtNombre.Text == "ISABELINES")
                        {
                            button.Size = new Size(tamaño, tamaño);
                            button.BackgroundImage = Resources.isabel;
                            button.SendToBack();
                        }
                        else
                        {
                            button.Size = new Size(tamaño, tamaño);
                            button.BackgroundImage = Resources.fantasma;
                            button.SendToBack();
                        }
                    }
                    else if (Tipo == Utiles.TipoBoton.SumarTiempo)
                    {
                        button.Size = new Size(tamaño, tamaño);
                        button.BackgroundImage = Resources.add_time;
                        button.SendToBack();

                    }
                    else if (Tipo == Utiles.TipoBoton.Bonus)
                    {
                        button.Size = new Size(tamaño, tamaño);
                        button.BackgroundImage = Resources.hiclipart_com;
                        button.SendToBack();

                    }
                    else
                    {
                        button.Size = new Size(tamaño, tamaño);
                        button.BackgroundImage = (Image)Resources.demonio;
                        button.BringToFront();
                    }

                    Controls.Add((Control)button);

                }
            }
        }

        private void clickBicho_Click(object sender, EventArgs e)
        {
            Utiles.TipoBoton tipoQueContiene = ((DatosObjeto)((Button)sender).Tag).Tipo;

            if (tipoQueContiene == Utiles.TipoBoton.Fantasma)
            {
                Utiles.ReproducirSonido(Utiles.TiposSonido.BeepCorto);
                CrearBicho(1, Utiles.TipoBoton.Fantasma);
                ++Clicks;
                if (txtNombre.Text == "ISABELINES")
                {
                    ++Clicks;
                }
            }
            else if (tipoQueContiene == Utiles.TipoBoton.SumarTiempo)
            {
                CrearBicho(1,Utiles.TipoBoton.SumarTiempo);
                Utiles.ReproducirSonido(Utiles.TiposSonido.SonBonus);
                Configuracion.SegundosJuego += 5;
            }
            else if (tipoQueContiene == Utiles.TipoBoton.Bonus)
            {
                CrearBicho(1,Utiles.TipoBoton.Bonus);
                Utiles.ReproducirSonido(Utiles.TiposSonido.SonBonus);
                Clicks += 3;
            }
            else
            {
                Utiles.ReproducirSonido(Utiles.TiposSonido.Booooo);
                CrearBicho(1,Utiles.TipoBoton.Diablo);
                --Clicks;
            }
          ((Component)sender).Dispose();
        }

        private void temporizador_Tick(object sender, EventArgs e)
        {
            Configuracion.SegundosJuego--;
            ActualizarSegundos();
            if(!Configuracion.UsarMovimiento && Configuracion.SegundosJuego % 2 == 0)
            {
                BorrarBichosRestantes(Utiles.TipoBoton.Diablo);
                CrearBicho(Configuracion.BichosMalos,Utiles.TipoBoton.Diablo);
            }

            
            if (Configuracion.SegundosJuego != 0)
                return;
            FinalizarJuego();
        }

        private void FinalizarJuego()
        {
            temporizador.Stop();
            Configuracion.SegundosJuego = Configuracion.SegundosIniciales;
            PuntosObtenidos = Math.Round(Convert.ToDouble(Clicks) / Convert.ToDouble(Configuracion.SegundosJuego) * 100.0);
            lblContadorClicks.Text = "¡FELICIDADES " + txtNombre.Text + " HAS CONSEGUIDO " + PuntosObtenidos.ToString() + " PUNTOS!";
            lblContadorClicks.Visible = true;
            temporizadorCrearBonus.Stop();
            SonidoSegunClicks();
            BorrarBichosRestantes(Utiles.TipoBoton.Todos);
            GuardarClasificacion();
            Application.DoEvents();
            Thread.Sleep(2000);
            btnStart.Visible = true;
            txtNombre.Visible = true;
            btnClasificaciones.Visible = true;
        }

        private void GuardarClasificacion()
        {
            using (BDEntities db = new BDEntities())
            {
                CLASIFICACIONES clasifNueva = new CLASIFICACIONES();
                clasifNueva.CONFIGURACION = 1;
                clasifNueva.DIFICULTAD = Configuracion.Dificultad == 3 ? "EXTREMO" : Configuracion.Dificultad == 1 ? "FACIL" : Configuracion.Dificultad == 2 ? "DIFICIL" : "PERSONALIZADO";
                clasifNueva.FECHA = DateTime.Now;
                clasifNueva.NOMBRE = txtNombre.Text;
                clasifNueva.PUNTUACION = PuntosObtenidos;
                db.CLASIFICACIONES.Add(clasifNueva);
                db.SaveChanges();
            }



            //string dificult = Configuracion.Dificultad == 0 ? "PERSONALIZADO" : Configuracion.Dificultad == 1 ? "FACIL" : Configuracion.Dificultad == 2 ? "DIFICIL" : "EXTREMO";
            //string contents = DateTime.Now.ToString("dd-MM-yyyy HH:mm") + ";" + txtNombre.Text + ";" + PuntosObtenidos.ToString() + ";" + dificult;
            //string directoryName = Path.GetDirectoryName(Utiles.RutaFichero);
            //if (!Directory.Exists(directoryName))
            //    Directory.CreateDirectory(directoryName);
            //if (File.Exists(Utiles.RutaFichero))
            //    File.AppendAllText(Utiles.RutaFichero, "\n" + contents);
            //else
            //    File.WriteAllText(Utiles.RutaFichero, contents);
        }

        private void SonidoSegunClicks()
        {
            if ((double)Clicks >= (double)Configuracion.SegundosIniciales * 1.5)
                Utiles.ReproducirSonido(Utiles.TiposSonido.Aplausos);
            else if ((double)Clicks < (double)Configuracion.SegundosIniciales * 0.8)
                Utiles.ReproducirSonido(Utiles.TiposSonido.EresUnManta);
            else
                Utiles.ReproducirSonido(Utiles.TiposSonido.Felicidades);
        }

        private void BorrarBichosRestantes(Utiles.TipoBoton TipoBicho)
        {
            if(TipoBicho !=Utiles.TipoBoton.Todos)
            {
                //Mientras queden controles tipo button en el form (compara el .Tag que no sea null y sea igual a tipobicho)
                while (Controls.OfType<Button>().Where(w => w.Tag != null && ((DatosObjeto)w.Tag).Tipo == TipoBicho).Count() > 0)
                {
                    //Por cada button en Controls de este form, hace lo de dentro del foreach: 
                    foreach (Button button in Controls.OfType<Button>())
                    {
                        //Si este boton.Tag es != null y el .Tag es igual a TipoBicho lo borramos.
                        if (button.Tag != null && ((DatosObjeto)button.Tag).Tipo == TipoBicho)
                        {
                            button.Dispose();
                        }
                    }
                }
            }
            else
            {
                //Mientras queden controles tipo button en el form (compara el .Tag que no sea null y sea igual a tipobicho)
                while (Controls.OfType<Button>().Where(w => w.Tag != null).Count() > 0)
                {
                    //Por cada button en Controls de este form, hace lo de dentro del foreach: 
                    foreach (Button button in Controls.OfType<Button>())
                    {
                        //Si este boton.Tag es != null y el .Tag es igual a TipoBicho lo borramos.
                        if (button.Tag != null)
                        {
                            button.Dispose();
                        }
                    }
                }

                //BORRAMOS LOS FALSOS CLICKS
                while (Controls.OfType<PictureBox>().Count() > 0)
                {
                    //Por cada imagen en Controls de este form, hace lo de dentro del foreach: 
                    foreach (PictureBox pb in Controls.OfType<PictureBox>())
                    {
                        pb.Dispose();
                    }
                }
            }

            

        }

        private void txtNombre_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "";
        }

        private void btnClasificaciones_Click(object sender, EventArgs e)
        {
            int num = (int)new FrmClasificaciones().ShowDialog();
        }

        private void temporizadorBonus_Tick(object sender, EventArgs e)
        {
            BorrarBichosRestantes(Utiles.TipoBoton.Bonus);
            BorrarBichosRestantes(Utiles.TipoBoton.SumarTiempo);
            temporizadorBonus.Stop();
        }

        private void temporizadorCrearBonus_Tick(object sender, EventArgs e)
        {
            if (Configuracion.UsarBonus) CrearBicho(1,Utiles.TipoBoton.Bonus);
            if (Configuracion.UsarTiemposExtra) CrearBicho(1,Utiles.TipoBoton.SumarTiempo);
            temporizadorBonus.Start();
            int proximaAparicion = Utiles.Aleatorio.Next(1000, 3000);
            temporizadorCrearBonus.Stop();
            temporizadorCrearBonus.Interval = proximaAparicion;
            temporizadorCrearBonus.Start();
        }

        private void timerMovimiento_Tick(object sender, EventArgs e)
        {
            if (Configuracion.IralAzar) Utiles.NosesaledelaPantallaenMovimiento(this, Configuracion);
            else Utiles.MoverseLinealmentePorLaPantalla(this, Configuracion);
        }
        private void FrmPunch_Click(object sender, EventArgs e)
        {
            if(Configuracion.Pintura && !btnStart.Visible)
            {
                PictureBox pb = new PictureBox();
                pb.Image = (Image)Properties.Resources.pintura;
                pb.SizeMode = PictureBoxSizeMode.Zoom;
                pb.Width = 50;
                pb.Height = 50;
                pb.Location = new Point(MousePosition.X -25, MousePosition.Y -25);
                Controls.Add(pb);
                pb.BringToFront();

            }
        }
    }
}
